﻿using UnityEngine;
using System;

[Obsolete]
public class NewCountdownTimer
{
    float timerLength;
    float timerStatus;

    public NewCountdownTimer(float newTimerLength, bool reset = true)
    {
        timerLength = newTimerLength;
        if(reset) ResetTimer();
    }

    public void
    ResetTimer()
    {
        timerStatus = timerLength;
    }

    public void
    PassOneFrame()
    {
        timerStatus -= Time.deltaTime;
    }

    float lastCall = 0;

    public void
    PassFrames()
    {
        timerStatus -= Time.time - lastCall;
        lastCall = Time.time;
    }

    public bool
    IsDone()
    {
        return timerStatus <= 0;
    }

    public float
    SecondsLeft()
    {
        return timerStatus;
    }

    public void
    SetAsDone()
    {
        timerStatus = 0f;
    }

    public float
    GetLength()
    {
        return timerLength;
    }
}