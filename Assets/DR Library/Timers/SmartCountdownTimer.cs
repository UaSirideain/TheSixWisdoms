﻿using UnityEngine;
using System;

[Obsolete("Not used any more", false)]
public class OldSmartCountdownTimer
{
    float timerLength;
    float timerStatus;

    public OldSmartCountdownTimer(float newTimerLength)
    {
        timerLength = newTimerLength;
        ResetTimer();
    }

    public void
    ResetTimer()
    {
        timerStatus = timerLength;
    }

    //public void
    //PassOneFrame()
    //{
    //    timerStatus -= Time.deltaTime;
    //}

    float lastCall = 0;

    void
    PassFrames()
    {
        timerStatus -= Time.time - lastCall;
        lastCall = Time.time;
    }

    public bool
    IsDone()
    {
        PassFrames();
        return timerStatus <= 0;
    }

    public float
    SecondsLeft()
    {
        return timerStatus;
    }

    public void
    SetAsDone()
    {
        timerStatus = 0f;
    }
}

public class NewSmartCountdownTimer
{
    float timerLength;
    float timerStatus;

    public NewSmartCountdownTimer(float newTimerLength, bool resetTimer = true)
    {
        timerLength = newTimerLength;
        if(resetTimer) ResetTimer();
    }

    public void
    ResetTimer()
    {
        timerStatus = timerLength;
    }

    void
    PassFrames()
    {
        timerStatus -= Time.deltaTime;
    }

    public bool
    IsDone()
    {
        PassFrames();
        return timerStatus <= 0;
    }

    public float
    SecondsLeft()
    {
        return timerStatus;
    }

    public void
    SetAsDone()
    {
        timerStatus = 0f;
    }
}