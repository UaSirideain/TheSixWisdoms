﻿using UnityEngine;

public class BidirectionalTimer
{
    float timerStatus;
    float minTime;
    float maxTime;

    public BidirectionalTimer(float newMinTime, float newMaxTime, bool startAtMaxTime = false)
    {
        minTime = newMinTime;
        maxTime = newMaxTime;

        if (!startAtMaxTime)    Min();
        else                    Max();
    }

    public void
    OneFrameForward(float multiplier = 1f)
    {
        timerStatus += Time.deltaTime * multiplier;
        ClampTimer();
    }

    public void
    OneFrameBackward(float multiplier = 1f)
    {
        timerStatus -= Time.deltaTime * multiplier;
        ClampTimer();
    }

    public float
    SecondsPassed()
    {
        return timerStatus;
    }

    public bool
    Maxed()
    {
        return timerStatus >= maxTime;
    }

    public bool
    Minned()
    {
        return timerStatus <= minTime;
    }

    public void
    Min()
    {
        timerStatus = minTime;
    }

    public void
    Max()
    {
        timerStatus = maxTime;
    }

    public float
    ReturnNormalizedProgress()
    {
        return timerStatus / maxTime;
    }

    void
    ClampTimer()
    {
        timerStatus = Mathf.Clamp(timerStatus, minTime, maxTime);
    }
}