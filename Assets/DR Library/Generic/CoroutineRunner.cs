﻿using UnityEngine;
using System.Collections;

namespace DR
{
    public class CoroutineRunner : MonoBehaviour
    {
        public static CoroutineRunner instance;

        void Awake()
        {
            instance = this;
        }

        public void
        StartChildCoroutine(IEnumerator coroutine)
        {
            StartCoroutine(coroutine);
        }
    }
}