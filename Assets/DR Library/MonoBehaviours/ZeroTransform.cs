﻿using UnityEngine;

namespace DR.MonoBehaviours
{
    public class ZeroTransform : MonoBehaviour
    {
        void
        OnValidate()
        {
            transform.position = new Vector3(0f, 0f, 0f);
        }
    }
}