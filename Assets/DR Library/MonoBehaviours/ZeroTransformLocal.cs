﻿using UnityEngine;

namespace DR.MonoBehaviours
{
    public class ZeroTransformLocal : MonoBehaviour
    {
        void
        OnValidate()
        {
            transform.localPosition = new Vector3(0f, 0f, 0f);
        }
    }
}