﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum PauseState { Play, Pause }

public class PauseManager : MonoBehaviour
{
    public static PauseManager Instance;

    [HideInInspector] public PauseState gameState;

    private List<IPausable> pausableObjects = new List<IPausable>();

    public void
    Subscribe(IPausable pausableObject)
    {
        if (!pausableObjects.Contains(pausableObject))
        {
            pausableObjects.Add(pausableObject);
        }
    }

    public void
    PauseGame()
    {
        Instance.gameState = PauseState.Pause;

        foreach (IPausable pausableObject in pausableObjects)
        {
            pausableObject?.Pause();
        }
    }

    public void
    ResumeGame()
    {
        Instance.gameState = PauseState.Play;

        foreach (IPausable pausableObject in pausableObjects)
        {
            pausableObject?.Resume();
        }
    }

    void Awake()
    {
        if (Instance != null && Instance != this)
        {
            Destroy(gameObject);
        }
        else
        {
            Instance = this;
        }
    }
}