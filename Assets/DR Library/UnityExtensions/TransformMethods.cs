﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
public static class TransformMethods
{
    public static float degreesTurnableIn1SecondCasual = 409f;

    public static void
    LookAt2D(this Transform t, Vector3 target)
    {
        Vector3 relative = t.InverseTransformPoint(target);
        float angle = Mathf.Atan2(relative.x, relative.y) * Mathf.Rad2Deg;
        t.Rotate(0f, 0f, -angle);
    }

    public static void
    LookAt2D(this Transform t, Transform target)
    {
        t.LookAt2D(target.position);
    }

    public static void
    LookAt2DOneFrame(this Transform t, Vector3 target)
    {
        if (!t.IsLookingAt(target))
        {
            Vector3 relative = t.InverseTransformPoint(target);
            float angle = Mathf.Atan2(relative.x, relative.y) * Mathf.Rad2Deg;

            if (angle == 0f) return;

            float turn;

            if (degreesTurnableIn1SecondCasual > Mathf.Abs(angle))
            {
                turn = -angle;
            }
            else
            {
                float turnDirection = angle / Mathf.Abs(angle);
                turn = -turnDirection * degreesTurnableIn1SecondCasual * Time.deltaTime;
            }

            t.Rotate(0f, 0f, turn);
        }
    }

    [Obsolete]
    public static void
    LookAt2DOneFrame(this Transform t, Vector3 target, float degreesTurnable = 409f)
    {

        if (!(degreesTurnable > 0))
        {
            degreesTurnable = degreesTurnableIn1SecondCasual;
        }
        if (!t.IsLookingAt(target))
        {
            Vector3 relative = t.InverseTransformPoint(target);
            float angle = Mathf.Atan2(relative.x, relative.y) * Mathf.Rad2Deg;
            float turnDirection = angle / Mathf.Abs(angle);
            t.Rotate(0f, 0f, -turnDirection * degreesTurnable * Time.deltaTime);
        }
    }

    public static bool
    IsLookingAt(this Transform t, Vector3 target)
    {
        Vector3 relative = t.InverseTransformPoint(target);
        float angle = Mathf.Atan2(relative.x, relative.y) * Mathf.Rad2Deg;
        return Mathf.Abs(angle) < 5f;
    }

    public static bool
    IsLookingAt(this Transform t, Vector3 target, float fov)
    {
        Vector3 relative = t.InverseTransformPoint(target);
        float angle = Mathf.Atan2(relative.x, relative.y) * Mathf.Rad2Deg;
        return Mathf.Abs(angle) < Mathf.Abs(fov * .5f);
    }
}