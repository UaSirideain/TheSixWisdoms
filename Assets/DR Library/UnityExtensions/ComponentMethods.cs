﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class ComponentMethods
{
    public static T
    FindComponentInParents<T>(this Transform t) where T : Component
    {
        if (!t) return null;

        t = t.parent;

        if (t.GetComponent<T>()) return t.GetComponent<T>();

        return t.FindComponentInParents<T>();
    }

    public static T
    FindComponentInChildren<T>(this Transform t) where T : Component
    {
        if (!t) return null;

        foreach (Transform child in t)
        {
            t = child;
            if (t.GetComponent<T>()) return t.GetComponent<T>();

            t.FindComponentInChildren<T>();
        }
        return null;
    }

    public static Transform
    FindNearest<T>(this Transform @this, List<T> targets, float distance) where T : Component
    {
        if (@this && targets.Count > 0)
        {
            Transform tempTarget = null;
            foreach (T target in targets)
            {
                if (target)
                {
                    if (Vector3.Distance(@this.position, target.transform.position) < distance)
                    {
                        distance = Vector3.Distance(@this.position, target.transform.position);
                        tempTarget = target.transform;
                    }
                }
            }
            return tempTarget;
        }
        else return null;
    }

    public static T
    GetComponentInTree<T>(this Transform @this) where T : Component
    {
        if (@this)
        {
            if (@this.GetComponent<T>())
            {
                return @this.GetComponent<T>();
            }
            else if (@this.FindComponentInChildren<T>())
            {
                return @this.FindComponentInChildren<T>();
            }
            else if (@this.FindComponentInParents<T>())
            {
                return @this.FindComponentInParents<T>();
            }
        }
        return null;
    }
}