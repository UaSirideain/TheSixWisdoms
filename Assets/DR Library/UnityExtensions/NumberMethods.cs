﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class NumberMethods
{
    public static int
    GetHighest(int a, int b)
    {
        if (a > b)
        {
            return a;
        }
        else
        {
            return b;
        }
    }

    public static float
    RandomBinomial()
    {
        return Random.value - Random.value;
    }
}