﻿using UnityEngine;

public static class ColourMethods
{
    public static Color
    Transparent(this Color c)
    {
        return new Color(c.r, c.g, c.b, 0f);
    }

    public static Color
    Opaque(this Color c)
    {
        return new Color(c.r, c.g, c.b, 1f);
    }

    public static Color
    SetOpacity(this Color c, float opacity)
    {
        return new Color(c.r, c.g, c.b, opacity);
    }
}