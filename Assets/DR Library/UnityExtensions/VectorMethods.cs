﻿using UnityEngine;
using DR.DataStructures;

public static class VectorMethods
{
    public static Vector3 nullv3 = new Vector3(-999f, -999f, -999f);
    public static Vector3 nullv2 = new Vector2(-999f, -999f);
    public static int nullInt = 9999;

    public static Quaternion scatterQ { get { return ScatterQ(); } }
    public static Vector3 scatter3 { get { return Scatter3(); } }


    public static Vector3 z1 = new Vector3(0f, 0f, 1f);

    public static Vector2
    v2(this Vector3 vector)
    {
        return (Vector2)vector;
    }

    public static Vector3
    v3(this Vector2 vector)
    {
        return (Vector3)vector;
    }

    public static Vector2
    Scatter2()
    {
        return new Vector3(UnityEngine.Random.Range(-1f, 1f), UnityEngine.Random.Range(-1f, 1f));
    }

    public static Vector3
    Scatter3()
    {
        return new Vector3(UnityEngine.Random.Range(-1f, 1f), UnityEngine.Random.Range(-1f, 1f), 0f);
    }

    public static Quaternion
    ScatterQ()
    {
        return Quaternion.Euler(0f, 0f, UnityEngine.Random.Range(0f, 360f));
    }

    public static float
    Distance<T1, T2>(T1 t1, T2 t2) where T1 : Component where T2 : Component
    {
        return t1 != null && t2 != null ? Vector3.Distance(t1.transform.position, t2.transform.position) : 0f;
    }

    public static Vector3
    GetDirectionTo(this Vector3 start, Vector3 end)
    {
        return (end - start).normalized;
    }

    public static Vector3
    GetDirectionTo(this Transform start, Vector3 end)
    {
        return (end - start.position).normalized;
    }

    public static Vector3
    GetDirectionTo(this Transform start, Transform end)
    {
        return (end.position - start.position).normalized;
    }

    public static float
    GetDistanceTo(this Transform start, Vector3 end)
    {
        return Vector3.Distance(start.position, end);
    }

    public static float
    GetDistanceTo(this Transform start, Transform end)
    {
        return Vector3.Distance(start.position, end.position);
    }

    public static float
    GetDistanceTo(this Vector2 start, Vector2 end)
    {
        return Vector2.Distance(start, end);
    }

    public static float
    GetDistanceTo(this Vector3 start, Vector3 end)
    {
        return ((Vector2)start).GetDistanceTo((Vector2)end);
    }

    public static bool
    IsToLeftOf(this Vector3 target, Transform vec)
    {
        return vec.InverseTransformPoint(target).x < 0f;
    }

    public static Vector2
    Rotate(this Vector2 vec, float angle)
    {
        float rad = angle * Mathf.Deg2Rad;

        float s = Mathf.Sin(rad);
        float c = Mathf.Cos(rad);

        return new Vector2(
            vec.x * c - vec.y * s,
            vec.y * c + vec.x * s
        );
    }

    public static Vector2
    Rotate(this Vector3 vec, float angle)
    {
        Vector2 copyVec = vec;

        float rad = -angle * Mathf.Deg2Rad;

        float s = Mathf.Sin(rad);
        float c = Mathf.Cos(rad);

        return new Vector3(
            copyVec.x * c - copyVec.y * s,
            copyVec.y * c + copyVec.x * s,
            0f
        );
    }

    public static float
    GetAngle(this Vector2 start, Vector2 target)
    {
        return Vector2.Angle(Vector2.up, target - start);
    }

    public static bool
    DoLinesIntersect(Line L1, Line L2, ref Point ptIntersection)
    {
        // Denominator for ua and ub are the same, so store this calculation
        double d =
           (L2.Y2 - L2.Y1) * (L1.X2 - L1.X1)
           -
           (L2.X2 - L2.X1) * (L1.Y2 - L1.Y1);

        //n_a and n_b are calculated as seperate values for readability
        double n_a =
           (L2.X2 - L2.X1) * (L1.Y1 - L2.Y1)
           -
           (L2.Y2 - L2.Y1) * (L1.X1 - L2.X1);

        double n_b =
           (L1.X2 - L1.X1) * (L1.Y1 - L2.Y1)
           -
           (L1.Y2 - L1.Y1) * (L1.X1 - L2.X1);

        // Make sure there is not a division by zero - this also indicates that
        // the lines are parallel.  
        // If n_a and n_b were both equal to zero the lines would be on top of each 
        // other (coincidental).  This check is not done because it is not 
        // necessary for this implementation (the parallel check accounts for this).
        if (d == 0)
            return false;

        // Calculate the intermediate fractional point that the lines potentially intersect.
        double ua = n_a / d;
        double ub = n_b / d;

        // The fractional point will be between 0 and 1 inclusive if the lines
        // intersect.  If the fractional calculation is larger than 1 or smaller
        // than 0 the lines would need to be longer to intersect.
        if (ua >= 0d && ua <= 1d && ub >= 0d && ub <= 1d)
        {
            ptIntersection.X = L1.X1 + (ua * (L1.X2 - L1.X1));
            ptIntersection.Y = L1.Y1 + (ua * (L1.Y2 - L1.Y1));
            return true;
        }
        //Debug.Log("Failed");
        return false;
    }
}