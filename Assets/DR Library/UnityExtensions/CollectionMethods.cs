﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class CollectionMethods
{
    //LISTS
    public static T
    First<T>(this List<T> @this) where T : Component
    {
        return @this[0];
    }

    public static T
    Last<T>(this List<T> @this) where T : Component //dangerous operation if list is empty. Replace with "GetLastIndex()"
    {
        return @this[@this.Count - 1];
    }

    public static T
    GetNext<T>(this List<T> list, T entry) where T : Component
    {
        int indexOfNext = -1;

        for (int i = 0; i < list.Count; i++)
        {
            if (list[i] == entry) indexOfNext = i + 1;

            if (i == indexOfNext) return list[i];
        }

        return entry;
    }

    public static T
    GetPrevious<T>(this List<T> list, T entry) where T : Component
    {
        int indexOfPrevious = -1;

        for (int i = Mathf.Clamp(list.Count - 1, 0, 9999); i >= 0; i--)
        {
            if (i == indexOfPrevious) return list[i];

            if (list[i] == entry) indexOfPrevious = i - 1;
        }

        return entry;
    }

    public static void
    SanitiseList<T>(this List<T> list)
    {
        if (list.Count == 0) return;

        for (int i = list.Count - 1; i > 0; i--)
        {
            if (list[i] == null)
            {
                list.RemoveAt(i);
            }
        }
    }

    public static bool
    ContainsIndex<T>(this T[,] array, int x, int y)
    {
        return array.GetLength(0) > x && array.GetLength(1) > y && x >= 0 && y >= 0;
    }

    //public static bool
    //ContainsIndex<T>(this List<T> list, int index)
    //{
    //    return index < list.Count && index > 0;
    //}

    public static bool
    ContainsIndex<T>(this List<T> array, int x)
    {
        return array != null ? array.Count > x && x >= 0 : false;
    }

    public static int
    GetLastIndex<T>(this List<T> list)
    {
        if (list.Count > 0) return list.Count - 1;
        else return 0;
    }

    public static void
    SafeAdd<T>(this List<T> list, T item)
    {
        if (!list.Contains(item))
        {
            list.Add(item);
        }
    }

    public static void
    SafeRemove<T>(this List<T> list, T item)
    {
        if (list.Contains(item))
        {
            list.Remove(item);
        }
    }


    //DICTIONARIES
    public static void
    Overwrite<T1, T2>(this Dictionary<T1, T2> dict, T1 t1, T2 t2)
    {
        if (dict.ContainsKey(t1))
        {
            dict[t1] = t2;
        }
        else
        {
            dict.Add(t1, t2);
        }
    }
}