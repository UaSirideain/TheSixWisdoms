﻿namespace DR.DataStructures
{
    public class RankedT<T>
    {
        public T item;
        public float priority;

        public RankedT(T newItem, float newRank)
        {
            item = newItem;
            priority = newRank;
        }
    }
}