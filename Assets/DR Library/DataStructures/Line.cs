﻿using UnityEngine;

namespace DR.DataStructures
{
    public class Line
    {
        public double X1;
        public double X2;
        public double Y1;
        public double Y2;

        public Line(Vector2 vec1, Vector2 vec2, string nameo = "Line")
        {
            X1 = vec1.x;
            Y1 = vec1.y;
            X2 = vec2.x;
            Y2 = vec2.y;

            //GameObject go = new GameObject();
            //go.transform.position = new Vector3((float)X1, (float)Y1, 0f);
            //go.name = nameo + " A";

            //GameObject go2 = new GameObject();
            //go2.transform.position = new Vector3((float)X2, (float)Y2, 0f);
            //go2.name = nameo + " B";
        }
    }
}