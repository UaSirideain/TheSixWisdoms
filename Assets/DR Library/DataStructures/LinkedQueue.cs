﻿using System;
using System.Collections.Generic;

namespace DR.DataStructures
{
    public class LinkedQueue<T>
    {
        LinkedList<T> items = new LinkedList<T>();
        LinkedList<RankedT<T>> rankedItems = new LinkedList<RankedT<T>>();

        public void
        Enqueue(T item, float priority)
        {
            for (var tempNode = rankedItems.First; tempNode != null; tempNode = tempNode.Next)
            {
                if (tempNode.Value.priority >= priority)
                {
                    rankedItems.AddBefore(tempNode, new RankedT<T>(item, priority));
                    items.AddFirst(item);
                    return;
                }
            }

            rankedItems.AddLast(new RankedT<T>(item, priority));
            items.AddFirst(item);
        }

        public int
        Count
        {
            get { return rankedItems.Count; }
        }

        public T
        Dequeue()
        {
            if (rankedItems.First == null)
            {
                throw new InvalidOperationException("...");
            }

            var item = rankedItems.First.Value.item;
            items.Remove(item);
            rankedItems.RemoveFirst();

            return item;
        }

        public void
        Remove(T item)
        {
            RankedT<T> itemToRemove;
            for (var tempNode = rankedItems.First; tempNode != null; tempNode = tempNode.Next)
            {
                if (Compare(tempNode.Value.item, item))
                {
                    itemToRemove = tempNode.Value;
                    rankedItems.Remove(itemToRemove);
                    return;
                }
            }
        }

        bool
        Compare(T x, T y)
        {
            return EqualityComparer<T>.Default.Equals(x, y);
        }

        public bool
        Contains(T item)
        {
            return items.Contains(item);
        }
    }
}