r: ireland
l: english

q: What was St. Patrick's profession during his time as a slave in Ireland?
a: A shepherd.
c: History

q: St. Patrick brought which technology to Ireland alongside Christianity?
a: Writing.
c: History

q: Traditionally, what was Ireland's fifth province?
a: Meath
c: History



q: The monks of Scellig Mhichíl invented which Catholic tradition?
a: Confession.
c: History

q: In what year did British forces take part in what would be known as The Burning of Cork?
a: 1920 - the exact dates were from 11th to 12th December 1920.
c: History

q: Who was known as <i>The Liberator?</i>
a: Daniel O'Connell
c: History

q: Who's epitaph reads 'He is now where fierce indignation can no longer tear his heart'?
a: Jonathan Swift, who is buried in St. Patrick's Cathedral, Dublin.
c: History

q: In which decade did Ireland join what later became the EU?
a: 1970s.
c: History





q: Seán Clárach Mac Domhnaill is known mostly for writing which song?
a: Mo Ghile Mear.
c: Literature

q: In Seamus Heaney's poem <i>The Early Purges</i>, what is the narrator's uncle doing?
a: Drowning kittens.
c: Literature



q: Through the machinations of atomic particle theory, what are the characters of Flann O'Brien's The Third Policeman slowly turning into?
a: Bicycles.
c: Literature

q: Name three of Ireland's Nobel Literature Prize winners.
a: William Butler Yeats; George Bernard Shaw; Samuel Beckett; Séamus Heaney.
c: Literature

q: How many children were turned into swans in the story The Children of Lir?
a: 4 - Fionnuala, Aodh, Fiachra and Conn.
c: Mythology

q: What is the poem <i>Pangur Bán</i> about?
a: A cat. It is the earliest written poem in the Irish language.
c: Literature



q: Which coastal island had a king until 2018?
a: Toraigh
c: Geography

q: What is the most northerly county in Ireland?
a: Donegal.
c: Geography

q: The flag of Connacht bears which two icons?
a: An eagle and ann arm wielding a sword.
c: Politics


### Entertainment ###


q: Father Ted was created by Graham Linehan and who else?
a: Arthur Mathews
c: TV

q: In Father Ted, Dougal's milk float couldn't go below how many miles per hour without blowing up?
a: 4.
c: TV

q: What instrument does Larry Mullen Jr. play?
a: Drums - he plays in U2.
c: Music

q: What instrument did Turlough O'Carolan play?
a: The harp.
c: Music

q: Christy Moore first gained fame as part of which band?
a: Planxty.
c: Music


### Sports ###


q: The GAA was founded in which year?
a: 1884
c: Sports


q: Christy Ring played hurling for which county?
a: Cork.
c: Sports

q: What nickname is given to Kilkenny teams?
a: The Cats.
c: Sports

q: In what county was George best born?
a: Antrim.
c: Sports

q: In what year did Mayo last win an All-Ireland?
a: 1951.
c: Sports


### Nature & Science ###


q: Is Sitka Spruce native to Ireland?
a: It is not.
c: Nature

q: As of 2020, forestry covers how much of Ireland's landmass? 10%, 15% or 25%?
a: 10%.
c: Nature

q: What was traditionally the capital of Ireland?
a: Tara in Meath.
c: History

q: According to legend, who is the oldest man in Ireland?
a: Fionntán.
c: Mythology

q: Who was Fionn mac Cumhaill's greatest enemy?
a: Goll mac Morna - who was also his greatest ally.
c: Mythology

q: Flann O'Brien was a pseudonym for whom?
a: Brian O'Nolan.
c: Literature

q: What was the proposal in Jonathan Swift's <i>A Modest Proposal?</i>
a: That the poor could alleviate their poverty by selling their children as food to the upper classes.
c: Literature

q: Who was Taoiseach during 1998?
a: Bertie Ahern - for Fianna Fáil.
c: Politics

q: Who was Taoiseach during 1995?
a: John Bruton - for Fine Gael.
c: Politics

q: Which band had a hit with <i>Linger</i> in 1993?
a: The Cranberries.
c: Music

q: In which year did Michael D. Higgins become president?
a: 2011.
c: Politics

q: Which playwright's middle names were <i>Fingal O'Flahertie Wills</i>?
a: Oscar Wilde.
c: Plays

q: What is historically the national flower of Ireland?
a: Flax - also know as Linseed.
c: History

q: Before Michael D. Higgins was president, in which party did he serve as a TD?
a: The Labour Party, - although early in his career, he unsuccessfully contested an election as a member of Fianna Fáil.
c: Politics

q: Who wrote the theme song to the 1995 film GoldenEye?
a: U2 - although it was performed by Tina Turner.
c: Film

q: What is the most common surname in Ireland?
a: Murphy - particularly common in County Cork.
c: Sociopolitics

q: How many times has Ireland won the Eurovision Song Contest?
a: 7.
c: Music

