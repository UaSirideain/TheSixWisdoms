﻿using System;
using System.IO;
using System.Linq;
using System.Collections.Generic;
using UnityEngine;

public static class Serialiser
{
    const string savePath = "/savedata.txt";

    public static void
    Serialise(string property, string value)
    {
        string[] lines = Deserialise().Split(new[] { Environment.NewLine }, StringSplitOptions.None);
        StreamWriter writer = new StreamWriter(Application.persistentDataPath + savePath, false);

        bool propertySaved = false;

        for (int i = 0; i < lines.Length; i++)
        {
            if (lines[i].Length < 1) continue;

            if (lines[i].Contains(property))
            {
                lines[i] = (property + "=" + value);
                propertySaved = true;
            }
            
            writer.WriteLine(lines[i]);
        }

        if (!propertySaved)
        {
            writer.WriteLine(property + "=" + value);
        }

        writer.Close();
    }

    static string
    Deserialise()
    {
        if (File.Exists(Application.persistentDataPath + savePath) == false) return "";

        StreamReader reader = new StreamReader(Application.persistentDataPath + savePath, true);
        string file = reader.ReadToEnd();
        reader.Close();
        return file;
    }

    public static string[]
    GetSaveFile()
    {
        if (File.Exists(Application.persistentDataPath + savePath) == false) return new string[]{""};

        StreamReader reader = new StreamReader(Application.persistentDataPath + savePath, true);
        string file = reader.ReadToEnd();
        reader.Close();
        return file.Split(new[] { Environment.NewLine }, StringSplitOptions.None);
    }

    public static string
    GetSaveFileSingleString()
    {
        if (File.Exists(Application.persistentDataPath + savePath) == false) return "";

        StreamReader reader = new StreamReader(Application.persistentDataPath + savePath, true);
        string file = reader.ReadToEnd();
        reader.Close();
        return file;
    }

    public static void
    DeleteSavedData()
    {
        foreach (string file in Directory.GetFiles(Application.persistentDataPath))
        {
            if (file.Contains(".txt")) File.Delete(file);
        }

        GameObject.FindObjectOfType<QuestionBank>().ReloadDecks();
    }
}