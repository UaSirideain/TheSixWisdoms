﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class SettingsScreen : MonoBehaviour
{
    QuestionParser parser;
    QuestionBank bank;

    [SerializeField] RectTransform deckCardPrefab;
    [SerializeField] RectTransform panel;

    [SerializeField] List<CategoryButton> buttons;

    public void
    Dehighlight()
    {
        foreach (CategoryButton button in buttons)
        {
            button.PointerExit();
        }
    }

    void
    Start()
    {
        parser = FindObjectOfType<QuestionParser>();
        bank = FindObjectOfType<QuestionBank>();

        List<TextAsset> decks = parser.GetAvailableDecks();

        for (int i = 0; i < decks.Count; i++)
        {
            TextAsset deck = decks[i];

            RectTransform newCard = Instantiate(deckCardPrefab);
            newCard.SetParent(panel, false);
            newCard.anchoredPosition = new Vector2(0f, -i * newCard.sizeDelta.y);
            newCard.GetComponentInChildren<Text>().text = deck.name;

            EventTrigger trigger = newCard.gameObject.AddComponent<EventTrigger>();

            EventTrigger.Entry pointerDownEntry = new EventTrigger.Entry();
            pointerDownEntry.eventID = EventTriggerType.PointerDown;
            pointerDownEntry.callback.AddListener((data) => { newCard.GetChild(2).gameObject.SetActive(parser.ActivateDeck(deck)); bank.ReloadDecks(); });
            trigger.triggers.Add(pointerDownEntry);
        }
    }
}