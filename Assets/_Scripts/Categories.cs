﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum Wisdom { Purple, Green, Orange, Blue, Yellow, Pink }

public enum Category
{
    Arts, Literature, Mythology,
    Nature, Science,
    Sports, Leisure,
    Geography, Sociopolitics,
    History,
    Entertainment, PopularCulture
}

public enum Topic
{ 
    Arts, Literature, Mythology, Plays, Poetry,
    TV, Film, Music, VideoGames, LordOfTheRings,
    History, Folklore,
    Geography, Politics, Religion, Economics, Sociopolitics, Language,
    Nature, Science, Physics, Biology, Maths, Chemistry, Animals, Astronomy, Psychology, Technology, Geology,
    Sports, GAA, Golf, Olympics, Soccer, Leisure, Boxing,
    Food
}


public static class Categories
{
    public static Dictionary<Wisdom, Color> wisdomToColour = new Dictionary<Wisdom, Color>()
    {
        { Wisdom.Purple,    new Color(.470f, .274f, .450f, 1f) },
        { Wisdom.Yellow,    new Color(.757f, .764f, .507f, 1f) },
        { Wisdom.Blue,      new Color(.372f, .460f, .462f, 1f) },
        { Wisdom.Orange,    new Color(.584f, .462f, .273f, 1f) },
        { Wisdom.Green,     new Color(.336f, .470f, .352f, 1f) },
        { Wisdom.Pink,      new Color(.735f, .475f, .700f, 1f) }
    };

    public static Dictionary<Category, Wisdom> categoryToWisdom = new Dictionary<Category, Wisdom>()
    {
        { Category.Arts,                Wisdom.Purple },
        { Category.Literature,          Wisdom.Purple },
        { Category.Mythology,           Wisdom.Purple },

        { Category.History,             Wisdom.Yellow },

        { Category.Geography,           Wisdom.Blue },
        { Category.Sociopolitics,       Wisdom.Blue },

        { Category.Sports,              Wisdom.Orange },
        { Category.Leisure,             Wisdom.Orange },

        { Category.Nature,              Wisdom.Green },
        { Category.Science,             Wisdom.Green },

        { Category.Entertainment,       Wisdom.Pink },
        { Category.PopularCulture,      Wisdom.Pink }
    };

    public static Dictionary<Topic, Category> topicToCategory = new Dictionary<Topic, Category>()
    {
        {Topic.Arts,               Category.Arts },

        {Topic.Literature,         Category.Literature },
        {Topic.Plays,              Category.Literature },
        {Topic.Poetry,             Category.Literature },

        {Topic.Mythology,          Category.Mythology },
        {Topic.Folklore,           Category.Mythology },

        {Topic.Nature,             Category.Nature },
        {Topic.Animals,            Category.Nature },

        {Topic.Science,            Category.Science },
        {Topic.Maths,              Category.Science },
        {Topic.Physics,            Category.Science },
        {Topic.Biology,            Category.Science },
        {Topic.Astronomy,          Category.Science },
        {Topic.Chemistry,          Category.Science },
        {Topic.Psychology,         Category.Science },
        {Topic.Geology,            Category.Science },

        {Topic.Technology,         Category.Science },

        {Topic.TV,                 Category.Entertainment },
        {Topic.Film,               Category.Entertainment },
        {Topic.Music,              Category.Entertainment },
        {Topic.VideoGames,         Category.Entertainment },
        {Topic.LordOfTheRings,     Category.Entertainment },

        {Topic.Sports,             Category.Sports },
        {Topic.Golf,               Category.Sports },
        {Topic.Soccer,             Category.Sports },
        {Topic.GAA,                Category.Sports },
        {Topic.Olympics,           Category.Sports },
        {Topic.Leisure,            Category.Leisure },
        {Topic.Boxing,             Category.Sports },

        {Topic.History,            Category.History },

        {Topic.Sociopolitics,      Category.Sociopolitics },
        {Topic.Religion,           Category.Sociopolitics },
        {Topic.Politics,           Category.Sociopolitics },
        {Topic.Economics,          Category.Sociopolitics },

        {Topic.Geography,          Category.Geography },
        {Topic.Language,           Category.Geography },

        {Topic.Food,               Category.Nature }
    };

    public static Dictionary<string, Topic> stringToTopic = new Dictionary<string, Topic>()
    {
        {"Arts",                Topic.Arts },
        {"Literature",          Topic.Literature },
        {"Mythology",           Topic.Mythology },
        {"Plays",               Topic.Plays },
        {"Poetry",              Topic.Poetry },

        {"Nature",              Topic.Nature },
        {"Science",             Topic.Science },
        {"Food",                Topic.Food },
        {"Psychology",          Topic.Psychology },
        {"Chemistry",           Topic.Chemistry },
        {"Physics",             Topic.Physics },
        {"Biology",             Topic.Biology },
        {"Maths",               Topic.Maths },
        {"Animals",             Topic.Animals },
        {"Astronomy",           Topic.Astronomy },
        {"Technology",          Topic.Technology },
        {"Geology",             Topic.Geology },

        {"TV",                  Topic.TV },
        {"Movies",              Topic.Film },
        {"Film",                Topic.Film },
        {"Music",               Topic.Music },
        {"Video Games",         Topic.VideoGames },
        {"Lord of the Rings",   Topic.LordOfTheRings },

        {"Sports",              Topic.Sports },
        {"GAA",                 Topic.GAA },
        {"Olympics",            Topic.Olympics },
        {"Golf",                Topic.Golf },
        {"Soccer",              Topic.Soccer },
        {"Leisure",             Topic.Leisure },
        {"Boxing",              Topic.Boxing },

        {"History",             Topic.History },
        {"Folklore",            Topic.Folklore },

        {"Sociopolitics",       Topic.Sociopolitics },
        {"Politics",            Topic.Politics },
        {"Geography",           Topic.Geography },
        {"Religion",            Topic.Religion },
        {"Economics",           Topic.Economics },
        {"Language",            Topic.Language }
    };

    public static Topic
    ToTopic(this string str)
    {
        return stringToTopic[str];
    }

    public static Category
    ToCategory(this Topic topic)
    {
        return topicToCategory[topic];
    }

    public static Wisdom
    ToWisdom(this Category category)
    {
        return categoryToWisdom[category];
    }

    public static Color
    ToColour(this Wisdom wisdom)
    {
        return wisdomToColour[wisdom];
    }
}