﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System;

public class StatsPanel : MonoBehaviour
{
    QuestionBank bank;

    [SerializeField] RectTransform topicStatPrefab;
    [SerializeField] RectTransform panel;

    void
    Start()
    {
        bank = FindObjectOfType<QuestionBank>();

        int positioner = 0;
        int rowAdjuster = 0;

        //for (int i = 0; i < Enum.GetNames(typeof(Topic)).Length; i++)
        //{
        //    Topic currentTopic = (Topic)i;
        //    RectTransform newCard = Instantiate(topicStatPrefab);

        //    newCard.SetParent(panel, false);

        //    float rawPosition = (positioner * newCard.sizeDelta.y);

        //    int row = (int)Mathf.Floor(rawPosition / panel.sizeDelta.y);

        //    float adjustedPosition = (positioner * newCard.sizeDelta.y) - (panel.sizeDelta.y * row);

        //    float x = row * newCard.sizeDelta.x;
        //    float y = -adjustedPosition;

        //    newCard.anchoredPosition = new Vector2(x, y);

        //    newCard.GetChild(0).GetComponent<Text>().text = currentTopic.ToString();
        //    int count = bank.CountQuestionsBy(currentTopic);
        //    newCard.GetChild(1).GetComponent<Text>().text = count.ToString();

        //    positioner++;
        //}

        for (int i = 0; i < Enum.GetNames(typeof(Wisdom)).Length; i++)
        {
            Wisdom currentCategory = (Wisdom)i;
            RectTransform newCard = Instantiate(topicStatPrefab);

            newCard.SetParent(panel, false);

            float rawPosition = (positioner * newCard.sizeDelta.y);

            int row = (int)Mathf.Floor(rawPosition / panel.sizeDelta.y);

            float x = row * newCard.sizeDelta.x;
            float y = -((positioner * newCard.sizeDelta.y) - (panel.sizeDelta.y * row));

            newCard.anchoredPosition = new Vector2(x, y);

            newCard.GetChild(0).GetComponent<Text>().text = currentCategory.ToString();
            newCard.GetChild(0).GetComponent<Text>().color = currentCategory.ToColour() * 1.3f;
            int count = bank.CountQuestionsBy(currentCategory);
            newCard.GetChild(1).GetComponent<Text>().text = count.ToString();
            newCard.GetChild(1).GetComponent<Text>().color = currentCategory.ToColour() * 1.3f;

            positioner++;
        }

        rowAdjuster++;
        positioner++;
        positioner++;
        positioner++;
        positioner++;
        positioner++;
        positioner++;
        positioner++;
        positioner++;

        for (int i = 0; i < Enum.GetNames(typeof(Category)).Length; i++)
        {
            Category currentCategory = (Category)i;
            RectTransform newCard = Instantiate(topicStatPrefab);

            newCard.SetParent(panel, false);

            float rawPosition = (positioner * newCard.sizeDelta.y);

            int row = (int)Mathf.Floor(rawPosition / panel.sizeDelta.y);

            float x = row * newCard.sizeDelta.x;
            float y = -((positioner * newCard.sizeDelta.y) - (panel.sizeDelta.y * row));

            newCard.anchoredPosition = new Vector2(x, y);

            newCard.GetChild(0).GetComponent<Text>().text = currentCategory.ToString();
            newCard.GetChild(0).GetComponent<Text>().color = currentCategory.ToWisdom().ToColour() * 1.3f;
            int count = bank.CountQuestionsBy(currentCategory);
            newCard.GetChild(1).GetComponent<Text>().text = count.ToString();
            newCard.GetChild(1).GetComponent<Text>().color = currentCategory.ToWisdom().ToColour() * 1.3f;

            positioner++;
        }

        rowAdjuster++;
        positioner++;
        positioner++;
        
        {
            RectTransform newCard = Instantiate(topicStatPrefab);

            newCard.SetParent(panel, false);

            float rawPosition = (positioner * newCard.sizeDelta.y);

            int row = (int)Mathf.Floor(rawPosition / panel.sizeDelta.y);

            float x = row * newCard.sizeDelta.x;
            float y = -((positioner * newCard.sizeDelta.y) - (panel.sizeDelta.y * row));

            newCard.anchoredPosition = new Vector2(x, y);

            newCard.GetChild(0).GetComponent<Text>().text = "Total";
            int count = bank.CountQuestions();
            newCard.GetChild(1).GetComponent<Text>().text = count.ToString();
        }
    }
}