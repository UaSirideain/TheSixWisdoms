﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public enum LanguageType { Gaeilge, English }

public class LanguageSettings : MonoBehaviour
{
    [SerializeField] Text gaeilgeText;
    [SerializeField] Text englishText;

    public void
    SelectGaeilge()
    {
        gaeilgeText.color = Color.white;
        englishText.color = new Color(1f, 1f, 1f, .31f);
        Language.ChangeLanguage(LanguageType.Gaeilge);
    }

    public void
    SelectEnglish()
    {
        englishText.color = Color.white;
        gaeilgeText.color = new Color(1f, 1f, 1f, .31f);
        Language.ChangeLanguage(LanguageType.English);
    }

    public void
    Awake()
    {
        Language.LoadData(Serialiser.GetSaveFile());

        if (Language.CurrentLanguage() == LanguageType.English)
        {
            englishText.color = Color.white;
            gaeilgeText.color = new Color(1f, 1f, 1f, .31f);
        }
        else
        {
            gaeilgeText.color = Color.white;
            englishText.color = new Color(1f, 1f, 1f, .31f);
        }
    }
}