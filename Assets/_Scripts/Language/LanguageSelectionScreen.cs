﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LanguageSelectionScreen : MonoBehaviour
{
    public void
    Awake()
    {
        string file = Serialiser.GetSaveFileSingleString();
        if (file.Contains("Language="))
        {
            gameObject.SetActive(false);
            Destroy(gameObject);
        }
    }
}