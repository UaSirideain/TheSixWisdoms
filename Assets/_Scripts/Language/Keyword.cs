﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Keyword : MonoBehaviour
{
    [SerializeField] Ling keyword;
    Text text;

    public void
    AssignKeyword()
    {
        text.text = Language.Translate(keyword);
    }

    void
    Awake()
    {
        text = GetComponent<Text>();
        Language.RegisterKeywordText(this);
        AssignKeyword();
    }
}