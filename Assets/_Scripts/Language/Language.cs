﻿using System.Collections;
using System;
using System.Collections.Generic;
using UnityEngine;

public enum Ling
{ 
    The6Wisdoms, TrivialPursuitCompanionApp, DevelopedBy, DigitalRagCredits, Fonts,
    ArtLitMyth, NatureScience, SportsLeisure,
    Art, Literature, Mythology, Nature, Science, Sports, Leisure, Geography, History, Entertainment,
    ClearSavedData, DataReset,
    InternationalQuestions, IrelandQuestions
}

public static class Language
{
    static bool languageSet = false;

    static LanguageType currentLanguage;

    public static LanguageType
    CurrentLanguage()
    {
        return currentLanguage;
    }

    static List<Keyword> keywords = new List<Keyword>();

    public static bool
    IsActiveLanguage(this string languageString)
    {
        if (languageString.Contains("gaeilge") || languageString.Contains("irish"))
        {
            return currentLanguage == LanguageType.Gaeilge;
        }
        else if (languageString.Contains("english"))
        {
            return currentLanguage == LanguageType.English;
        }
        else
        {
            Debug.Log("Error: Language string not valid: " + languageString);
            return false;
        }
    }

    public static Dictionary<Ling, string> gaeilgeKeywords = new Dictionary<Ling, string>()
    {
        {Ling.The6Wisdoms, "Na 6 Gaois" },
        {Ling.TrivialPursuitCompanionApp, "Comhaip le Trivial Pursuit" },
        {Ling.DigitalRagCredits, "<i>Déanta in Éirinn ag:</i>\n\n<b>Brian Ó Sioradáin</b>\nForbairt Aip\nCeisteanna\n\n<b>Niall Ó Colmáin</b>\nForbairt Aip\nCeisteanna\n\n<b>Eoin Ó Colmáin</b>\nCeisteanna" },
        {Ling.Fonts, "<b>Clónna:</b>\nIrish Penny\nRaleway" },
        {Ling.DevelopedBy, "forbartha ag digitalrag.net" },

        {Ling.ArtLitMyth, "Ealaín, Litríocht\n& Miotaseolaíocht" },
        {Ling.NatureScience, "Dúlra & Eolas" },
        {Ling.SportsLeisure, "Spórt" },

        {Ling.Art, "Ealaín" },
        {Ling.Literature, "Litríocht" },
        {Ling.Mythology, "Miotaseolaíocht" },
        {Ling.Nature, "Dúlra" },
        {Ling.Science, "Eolas" },
        {Ling.Sports, "Spórt" },
        {Ling.Leisure, "Fóillíocht" },
        {Ling.Geography, "Tíreolaíocht" },
        {Ling.History, "Stair" },
        {Ling.Entertainment, "Siamsaíocht" },

        {Ling.ClearSavedData, "Scrios Sonraí Sabháilte" },
        {Ling.DataReset, "Sonraí Athchurtha" },

        {Ling.InternationalQuestions, "Ceisteanna Idirnaisiúnta" },
        {Ling.IrelandQuestions, "Ceisteanna Éireannacha" }
    };

    public static Dictionary<Ling, string> englishKeywords = new Dictionary<Ling, string>()
    {
        {Ling.The6Wisdoms, "The 6 Wisdoms" },
        {Ling.TrivialPursuitCompanionApp, "Trivial Pursuit Companion App" },
        {Ling.DigitalRagCredits, "<i>Made in Ireland by:</i>\n\n<b>Brian Ó Sioradáin</b>\nApp Development\nQuestions\n\n<b>Niall Ó Colmáin</b>\nApp Development\nQuestions\n\n<b>Eoin Ó Colmáin</b>\nQuestions" },
        {Ling.Fonts, "<b>Fonts:</b>\n\nIrish Penny\nRaleway" },
        {Ling.DevelopedBy, "developed by digitalrag.net" },

        {Ling.ArtLitMyth, "Art, Literature\n& Mythology" },
        {Ling.NatureScience, "Nature & Science" },
        {Ling.SportsLeisure, "Sport & Leisure" },

        {Ling.Art, "Art" },
        {Ling.Literature, "Literature" },
        {Ling.Mythology, "Mythology" },
        {Ling.Nature, "Nature" },
        {Ling.Science, "Science" },
        {Ling.Sports, "Sports" },
        {Ling.Leisure, "Leisure" },
        {Ling.Geography, "Geography" },
        {Ling.History, "History" },
        {Ling.Entertainment, "Entertainment" },

        {Ling.ClearSavedData, "Clear Saved Data" },
        {Ling.DataReset, "Data Reset" },

        {Ling.InternationalQuestions, "International Questions" },
        {Ling.IrelandQuestions, "Irish-regional Questions" }
};

    public static Dictionary<Ling, string>
    Directory()
    {
        if (currentLanguage == LanguageType.Gaeilge)
        {
            return gaeilgeKeywords;
        }
        else
        {
            return englishKeywords;
        }
    }

    public static void
    ChangeLanguage(LanguageType newLanguage)
    {
        currentLanguage = newLanguage;

        foreach (Keyword keyword in keywords)
        {
            keyword.AssignKeyword();
        }

        languageSet = true;

        GameObject.FindObjectOfType<QuestionBank>().ReloadDecks();
        SaveData();
    }

    static void
    SaveData()
    {
        Serialiser.Serialise("Language", currentLanguage.ToString());
    }

    public static void
    LoadData(string[] file)
    {
        foreach (string str in file)
        {
            if (str.Contains("Language"))
            {
                foreach (LanguageType newLanguage in (LanguageType[])Enum.GetValues(typeof(LanguageType)))
                {
                    if (str.Contains(newLanguage.ToString())) ChangeLanguage(newLanguage);
                }
            }
        }
    }

    public static void
    RegisterKeywordText(Keyword newKeyword)
    {
        keywords.SafeAdd(newKeyword);
    }

    public static string
    Translate(this Ling ling)
    {
        if (Directory().ContainsKey(ling))
        {
            return Directory()[ling];
        }
        else
        {
            Debug.Log(ling);
            return "<string not translated>";
        }
    }
}