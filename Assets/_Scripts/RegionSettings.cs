﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class RegionSettings : MonoBehaviour
{
    [SerializeField] Image internationalTickMark;
    [SerializeField] Image irishTickMark;

    public void
    ToggleIreland()
    {
        if (RegionType.Ireland.IsActiveRegion() == false)
        {
            Region.Add(RegionType.Ireland);
            irishTickMark.color = Color.white.Opaque();
        }
        else if (Region.Regions().Count > 1)
        {
            Region.Remove(RegionType.Ireland);
            irishTickMark.color = Color.white.Transparent();
        }

        Reset();
    }

    public void
    ToggleInternational()
    {
        if (RegionType.International.IsActiveRegion() == false )
        {
            Region.Add(RegionType.International);
            internationalTickMark.color = Color.white.Opaque();
        }
        else if(Region.Regions().Count > 1)
        {
            Region.Remove(RegionType.International);
            internationalTickMark.color = Color.white.Transparent();
        }

        Reset();
    }

    public void
    Awake()
    {
        Region.Deserialise(Serialiser.GetSaveFile());
        internationalTickMark.color = RegionType.International.IsActiveRegion() ? Color.white : Color.white.Transparent();
        irishTickMark.color = RegionType.Ireland.IsActiveRegion() ? Color.white : Color.white.Transparent();
    }

    void
    Reset()
    {
        FindObjectOfType<QuestionBank>().ReloadDecks();
    }
}