﻿using System.Collections;
using System;
using System.Collections.Generic;
using UnityEngine;

public enum RegionType { International, Ireland }

public static class Region
{
    static List<RegionType> activeRegions = new List<RegionType>()
    {
        RegionType.Ireland, RegionType.International
    };

    public static List<RegionType>
    Regions()
    {
        List<RegionType> regions = new List<RegionType>();
        regions.AddRange(activeRegions);
        return regions;
    }

    static void
    SaveRegionData()
    {
        foreach (RegionType region in (RegionType[])Enum.GetValues(typeof(RegionType)))
        {
            Serialiser.Serialise(region.ToString(), activeRegions.Contains(region).ToString());
        }
    }

    public static void
    Deserialise(string[] file)
    {
        activeRegions = new List<RegionType>() {RegionType.Ireland, RegionType.International};

        foreach (string str in file)
        {
            foreach (RegionType region in (RegionType[])Enum.GetValues(typeof(RegionType)))
            {
                if (str.Contains(region.ToString()))
                {
                    if (str.Contains(true.ToString()))
                    {
                        activeRegions.SafeAdd(region);
                    }
                    else
                    {
                        activeRegions.SafeRemove(region);
                    }
                }
            }
        }
    }

    public static void
    Add(RegionType region)
    {
        activeRegions.SafeAdd(region);
        SaveRegionData();
    }

    public static void
    Remove(RegionType region)
    {
        activeRegions.SafeRemove(region);
        SaveRegionData();
    }

    public static bool
    IsActiveRegion(this RegionType region)
    {
        return activeRegions.Contains(region);
    }

    public static bool
    IsActiveRegion(this string region)
    {
        return (region.Contains("international") && activeRegions.Contains(RegionType.International)) || (region.Contains("ireland") && activeRegions.Contains(RegionType.Ireland));
    }
}