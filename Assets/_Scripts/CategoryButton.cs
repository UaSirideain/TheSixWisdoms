﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CategoryButton : MonoBehaviour
{
    MaskableGraphic image;
    Color defaultColour;

    [SerializeField] float adjustment = 1.3f;
    [SerializeField] bool startedHighlighted = false;

    public void
    PointerEnter()
    {
        image.color = defaultColour * adjustment;
    }

    public void
    PointerExit()
    {
        image.color = defaultColour;
    }

    void
    Awake()
    {
        image = GetComponent<MaskableGraphic>();
        defaultColour = image.color;

        if (startedHighlighted) PointerEnter();
    }
}