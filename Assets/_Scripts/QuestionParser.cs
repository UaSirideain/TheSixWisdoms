﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System.IO;
using System;

public class QuestionParser : MonoBehaviour
{
    [SerializeField] List<TextAsset> decks;
    List<TextAsset> activeDecks = new List<TextAsset>();

    public List<TextAsset>
    GetAvailableDecks()
    {
        return decks;
    }

    public List<QuestionData>
    RetrieveQuestions()
    {
        activeDecks.Shuffle();

        List<QuestionData> questions = new List<QuestionData>();

        foreach (TextAsset txt in activeDecks)
        {
            questions.AddRange(Parse(txt));
        }

        questions.Shuffle();
        return questions;
    }

    public bool
    ActivateDeck(TextAsset newActiveDeck)
    {
        if (activeDecks.Contains(newActiveDeck))
        {
            if (activeDecks.Count > 1)
            {
                activeDecks.Remove(newActiveDeck);
            }
        }
        else
        {
            activeDecks.Add(newActiveDeck);
        }

        return activeDecks.Contains(newActiveDeck);
    }


    void
    Awake()
    {
        activeDecks.Clear();
        activeDecks.AddRange(decks);
    }

    List<QuestionData>
    Parse(TextAsset deckToParse)
    {
        List<QuestionData> questions = new List<QuestionData>();

        if (!File.Exists(Application.persistentDataPath + "/" + deckToParse.name + ".txt"))
        {
            StreamWriter writer = new StreamWriter(Application.persistentDataPath + "/" + deckToParse.name + ".txt", true);
            string originalText = deckToParse.ToString().Replace("c:", "s:" + Environment.NewLine + "c:");
            writer.Write(originalText);
            writer.Close();
        }

        string text = "";

        StreamReader reader = new StreamReader(Application.persistentDataPath + "/" + deckToParse.name + ".txt", true);
        text = reader.ReadToEnd();
        reader.Close();

        string[] lines = text.Split(new[] { Environment.NewLine }, StringSplitOptions.None);

        string question = "";
        string answer = "";
        int timesSeen = 0;
        string category = "";


        foreach (string line in lines)
        {
            if (line.Length > 0)
            {
                if (ValidateLanguageAndRegion(line) == false) return questions;

                if (line.StartsWith("q")) question = line.Replace("q:", "");
                if (line.StartsWith("a")) answer = line.Replace("a:", "");
                if (line.StartsWith("s"))
                {
                    if (int.TryParse(line.Replace("s: ", ""), out timesSeen) == false)
                    {
                        timesSeen = 0;
                    }
                }
                if (line.StartsWith("c"))
                {
                    category = line.Replace("c:", "");
                    questions.Add(new QuestionData(question.Trim(), answer.Trim(), category.Trim(), deckToParse.name, timesSeen));
                }
            }
        }

        questions.Shuffle();
        return questions;
    }

    bool
    ValidateLanguageAndRegion(string line)
    {
        if (line.StartsWith("l"))
        {
            string languageMarker = line.Replace("l: ", "");
            return languageMarker.IsActiveLanguage();
        }
        else if (line.StartsWith("r"))
        {
            string regionMarker = line.Replace("r: ", "");
            //if(regionMarker.IsActiveRegion() == false) GameObject.Find("Error Text").GetComponent<Text>().text = regionMarker;
            return regionMarker.IsActiveRegion();
        }
        else
        {
            return true;
        }
    }
}