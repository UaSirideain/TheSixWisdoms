﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DataSettings : MonoBehaviour
{
    public void
    ClearSavedData()
    {
        Serialiser.DeleteSavedData();
        FindObjectOfType<RegionSettings>().Awake();
        FindObjectOfType<LanguageSettings>().Awake();
        GameObject.FindObjectOfType<QuestionBank>().ReloadDecks();
    }
}