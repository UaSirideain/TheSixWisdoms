﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UIScreen : MonoBehaviour
{
    CanvasGroup canvas;

    public void
    Close()
    {
        canvas.blocksRaycasts = false;
        StartCoroutine(FadeIn(canvas.alpha, 0f));
    }

    public void
    Open()
    {
        canvas.blocksRaycasts = true;
        StartCoroutine(FadeIn(canvas.alpha, 1f));
    }

    IEnumerator
    FadeIn(float start, float end)
    {
        float t = 0f;

        while (t < 1f)
        {
            canvas.alpha = Mathf.Lerp(start, end, t);
            t += Time.deltaTime * 10f;
            yield return null;
        }

        canvas.alpha = end;
    }

    void
    Awake()
    {
        canvas = GetComponent<CanvasGroup>();
    }
}