﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using System.Linq;

public class QuestionBank : MonoBehaviour
{
    public int questionsInBank = 0;

    QuestionParser parser;

    List<QuestionData> bank = new List<QuestionData>();

    public QuestionData
    GetQuestion(Wisdom requestedColour)
    {
        if (bank.Count == 0)
        {
            ReloadDecks();
        }

        foreach (QuestionData question in bank)
        {
            Category broad = Categories.topicToCategory[question.topic];
            Wisdom questionColour = Categories.categoryToWisdom[broad];
            if (questionColour == requestedColour)
            {
                QuestionData retrievedQuestion = question;
                RemoveQuestion(question);
                return retrievedQuestion;
            }
        }

        return new QuestionData("No <i>" + GetRegionalString() + "</i> questions could be found in the <i>" + requestedColour.ToString().ToLower() + "</i> category for the <i>" + Language.CurrentLanguage().ToString() + "</i> language.", "", "Animals", "", 0);
    }

    string
    GetRegionalString()
    {
        string str = "";

        foreach (RegionType region in Region.Regions())
        {
            str += region.ToString().ToLower() + "</i> or <i>";
        }

        str = str.Remove(str.Length - 11);

        return str;
    }

    public void
    ReloadDecks()
    {
        bank.Clear();
        bank.AddRange(parser.RetrieveQuestions());
        SortBankByFrequency();
    }

    public int
    CountQuestionsBy(Topic topic)
    {
        int total = 0;
        foreach (QuestionData question in bank)
        {
            if (question.topic == topic)
            {
                total++;
            }
        }
        return total;
    }

    public int
    CountQuestionsBy(Category topic)
    {
        int total = 0;
        foreach (QuestionData question in bank)
        {
            if (question.topic.ToCategory() == topic)
            {
                total++;
            }
        }
        return total;
    }

    public int
    CountQuestionsBy(Wisdom topic)
    {
        int total = 0;
        foreach (QuestionData question in bank)
        {
            if (question.topic.ToCategory().ToWisdom() == topic)
            {
                total++;
            }
        }
        return total;
    }

    public int
    CountQuestions()
    {
        return bank.Count;
    }

    void
    Awake()
    {
        parser = GetComponent<QuestionParser>();
        ReloadDecks();
    }

    void
    RemoveQuestion(QuestionData questionToRemove)
    {
        bank.Remove(questionToRemove);
    }

    void
    SortBankByFrequency()
    {
        bank = bank.OrderBy(x => x.timesSeen).ToList();
    }
}