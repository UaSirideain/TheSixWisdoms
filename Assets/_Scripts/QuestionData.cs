﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;
using System;

public class QuestionData : System.Object, IComparable<QuestionData>
{
    public readonly string question;
    public readonly string answer;
    public readonly Topic topic;
    public int timesSeen;
    public readonly string fileName;

    public QuestionData(string questionText, string answerText, Topic newTopic, string newFileName, int newTimesSeen)
    {
        question = questionText;
        answer = answerText;
        topic = newTopic;
        fileName = newFileName;
        timesSeen = newTimesSeen;
    }

    public QuestionData(string questionText, string answerText, string topicString, string newFileName, int newTimesSeen)
    {
        question = questionText;
        answer = answerText;
        fileName = newFileName;
        timesSeen = newTimesSeen;

        if (Categories.stringToTopic.TryGetValue(topicString, out Topic retrievedTopic))
        {
            topic = retrievedTopic;
        }
        else
        {
            Debug.Log(topicString);// + ": (" + questionText + ")");
        }
    }

    public int
    CompareTo(QuestionData data)
    {
        if(timesSeen > data.timesSeen)      return  1;
        if(timesSeen == data.timesSeen)     return  0;
        /*if(timesSeen < data.timesSeen)*/  return -1;
    }

    public void
    ViewAndSave()
    {
        timesSeen++;

        //get deck from other source
        if (File.Exists(Application.persistentDataPath + "/" + fileName + ".txt"))
        {
            StreamReader reader = new StreamReader(Application.persistentDataPath + "/" + fileName + ".txt", true);
            string[] lines = reader.ReadToEnd().Split(new[] { Environment.NewLine }, StringSplitOptions.None);
            reader.Close();

            for (int i = 0; i < lines.Length; i++)
            {
                if (lines[i].StartsWith("q") && lines[i].Contains(question))
                {
                    lines[i + 2] = "s: " + timesSeen.ToString();
                }
            }

            StreamWriter writer = new StreamWriter(Application.persistentDataPath + "/" + fileName + ".txt", false);
            string text = string.Join(Environment.NewLine, lines);
            writer.Write(text);
            writer.Close();
        }
    }
}