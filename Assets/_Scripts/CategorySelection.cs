﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CategorySelection : MonoBehaviour
{
    [SerializeField] QuestionBank questionBank;
    [SerializeField] QuestionScreen questionScreen;

    public void
    SelectPurple()
    {
        QuestionData question = questionBank.GetQuestion(Wisdom.Purple);
        questionScreen.SetQuestionAndAnswer(question);
    }

    public void
    SelectGreen()
    {
        QuestionData question = questionBank.GetQuestion(Wisdom.Green);
        questionScreen.SetQuestionAndAnswer(question);
    }

    public void
    SelectOrange()
    {
        QuestionData question = questionBank.GetQuestion(Wisdom.Orange);
        questionScreen.SetQuestionAndAnswer(question);
    }

    public void
    SelectBlue()
    {
        QuestionData question = questionBank.GetQuestion(Wisdom.Blue);
        questionScreen.SetQuestionAndAnswer(question);
    }

    public void
    SelectYellow()
    {
        QuestionData question = questionBank.GetQuestion(Wisdom.Yellow);
        questionScreen.SetQuestionAndAnswer(question);
    }

    public void
    SelectPink()
    {
        QuestionData question = questionBank.GetQuestion(Wisdom.Pink);
        questionScreen.SetQuestionAndAnswer(question);
    }
}