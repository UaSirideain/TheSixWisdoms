﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class QuestionScreen : MonoBehaviour
{
    CanvasGroup canvas;
    Image background;

    [SerializeField] Text categoryHeading;
    [SerializeField] Text question;
    [SerializeField] Text answer;

    [SerializeField] GameObject answerPanel;
    [SerializeField] GameObject revealButton;

    public void
    SetQuestionAndAnswer(QuestionData questionData)
    {
        ResetAnswerPanel();

        //categoryHeading.text = Categories.colourToName[Categories.narrowToBroad[questionData.category]];
        question.text = questionData.question;
        answer.text = questionData.answer;

        Category broad = Categories.topicToCategory[questionData.topic];
        Wisdom colour = Categories.categoryToWisdom[broad];
        background.color = Categories.wisdomToColour[colour];

        //background.color = Categories.categoryToColour[Categories.narrowToBroad[questionData.category]];

        StartCoroutine(FadeIn(0f, 1f));
        canvas.blocksRaycasts = true;
        questionData.ViewAndSave();
    }

    public void
    Close()
    {
        StartCoroutine(FadeIn(1f, 0f));
        canvas.blocksRaycasts = false;
    }

    void
    ResetAnswerPanel()
    {
        answerPanel.SetActive(false);
        revealButton.SetActive(true);
    }

    public void
    RevealAnswer()
    {
        answerPanel.SetActive(true);
        revealButton.SetActive(false);
    }

    void
    Awake()
    {
        canvas = GetComponent<CanvasGroup>();
        background = GetComponent<Image>();
    }

    IEnumerator
    FadeIn(float start, float end)
    {
        float t = 0f;

        while (t < 1f)
        {
            canvas.alpha = Mathf.Lerp(start, end, t);
            t += Time.deltaTime * 10f;
            yield return null;
        }

        canvas.alpha = end;
    }
}